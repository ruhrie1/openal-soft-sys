use std::{path::PathBuf, env};

extern crate bindgen;

fn main() {
    let dst = cmake::build("openal-soft");
    println!("cargo:rustc-link-search=native={}/lib64", dst.display());
    println!("cargo:rustc-link-lib=openal");
    println!("cargo:rerun-if-changed=build.rs");

    let bindings = bindgen::Builder::default()
        .header("openal-soft/include/AL/al.h")
        .header("openal-soft/include/AL/alc.h")
        .header("openal-soft/include/AL/alext.h")
        .header("openal-soft/include/AL/efx-creative.h")
        .header("openal-soft/include/AL/efx-presets.h")
        .header("openal-soft/include/AL/efx.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings");
}